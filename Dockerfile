FROM golang:1.13

WORKDIR /usr/src/oauth-adapter

COPY go.mod .

RUN go mod download

COPY . .

RUN go build

CMD ["./chula-sso-oauth-adapter"]
