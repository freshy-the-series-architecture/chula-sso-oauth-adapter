package constant

import (
	"regexp"

	"github.com/kelseyhightower/envconfig"
)

// Config : environment variable structure
type Config struct {
	// application port
	Port      string `default:":8080"`
	JWTSecret string `default:"ccmRwmujLb" split_words:"true"`

	// sso arg name to be convert to oauth protocol
	SSORedirectUriArgName string `envconfig:"SSO_REDIRECT_URI_ARG_NAME" default:"service"`
	SSOCodeArgName        string `envconfig:"SSO_CODE_ARG_NAME" default:"ticket"`

	// field to be encode in id_token jwt
	// {
	// 	"uid":"5b67f968a7b11b0001f6cdc0",
	// 	"username":"60310175",
	// 	"gecos":"Tanakorn Pisnupoomi, ENG",
	// 	"email":"6031017521@student.chula.ac.th",
	// 	"disable":false,
	// 	"roles":["student"],
	// 	"firstname":"Tanakorn",
	// 	"lastname":"Pisnupoomi",
	// 	"firstnameth":"ธนกร",
	// 	"lastnameth":"พิศนุภูมิ",
	// 	"ouid":"6031017521"
	// }
	Fields         map[string]string `default:"email:email,name:gecos" split_words:"true"`
	CheckList      []string          `split_words:"true"`
	CheckRegexList []*regexp.Regexp
	CheckAction    string `default:"blacklist" split_words:"true"`

	// api path
	AuthPath     string `default:"/auth" split_words:"true"`
	CallbackPath string `default:"/callback" split_words:"true"`
	TokenPath    string `default:"/token" split_words:"true"`

	// externalName
	ExternalName string `default:"localhost:8080" split_words:"true"`
}

var config Config

func init() {
	// parse environment variable with envconfig
	if err := envconfig.Process("OAUTH_GATEWAY", &config); err != nil {
		panic(err)
	}

	config.CheckRegexList = []*regexp.Regexp{}
	for _, deny := range config.CheckList {
		r, err := regexp.Compile(deny)
		if err != nil {
			panic(err)
		}
		config.CheckRegexList = append(config.CheckRegexList, r)
	}

}

// GetConfig : get environment variable
func GetConfig() Config {
	return config
}
