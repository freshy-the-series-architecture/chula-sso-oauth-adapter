package handler

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
)

type tokenValidateBody struct {
	Code string `form:"code"`
}

func TokenValidate(c echo.Context) error {

	var strtok []string
	var userInfo map[string]string
	userInfoClaim := jwt.MapClaims{}

	reqBody := new(tokenValidateBody)
	if err := c.Bind(reqBody); err != nil {
		c.Error(err)
		return c.String(400, "Bad request")
	}

	authHeader := c.Request().Header["Authorization"]
	if len(authHeader) == 1 {
		strtok = strings.Split(authHeader[0], " ")
	}

	if len(authHeader) != 1 || len(strtok) != 2 {
		return c.String(400, "Unable to decode given app info")
	}

	encodedAppInfo := strtok[1]
	decodedBytes, err := base64.StdEncoding.DecodeString(encodedAppInfo)
	if err != nil {
		return c.String(400, "Unable to decode given app info")
	}

	decodedBytesAppInfo := bytes.Split(decodedBytes, []byte(":"))
	appID := string(decodedBytesAppInfo[0])
	appSecret := string(decodedBytesAppInfo[1])

	ticketValidateRequest, err := http.NewRequest("GET", "https://account.it.chula.ac.th/serviceValidation", nil)

	if err != nil {
		return c.String(500, "Unable preparing token validation")
	}

	ticketValidateRequest.Header.Set("DeeAppId", appID)
	ticketValidateRequest.Header.Set("DeeAppSecret", appSecret)
	ticketValidateRequest.Header.Set("DeeTicket", reqBody.Code)

	httpClient := &http.Client{}
	resp, err := httpClient.Do(ticketValidateRequest)

	if err != nil || resp.StatusCode != 200 {
		fmt.Printf("[Error] token validation error: %d, %s", resp.StatusCode, resp.Body)
		return c.String(500, "Unable to validate token")
	}

	json.NewDecoder(resp.Body).Decode(&userInfo)

	checkedFlag := false
	for _, checkRegex := range appConfig.CheckRegexList {
		if checkRegex.MatchString(userInfo["ouid"]) {

			if appConfig.CheckAction == "blacklist" {
				return c.String(403, "Access denied")
			}

			// User id in white-list
			checkedFlag = true
			break

		}
	}

	if !checkedFlag && appConfig.CheckAction != "blacklist" {
		return c.String(403, "Access denied")
	}

	for dst, src := range appConfig.Fields {
		userInfoClaim[dst] = userInfo[src]
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, userInfoClaim)

	tokenString, err := token.SignedString([]byte(appConfig.JWTSecret))

	return c.JSON(200, &map[string]string{
		"access_token": tokenString,
		"id_token":     tokenString,
	})

}
