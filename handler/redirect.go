package handler

import (
	"strings"

	"github.com/labstack/echo"
	"gitlab.com/freshy-the-series-architecture/chula-sso-oauth-adapter/constant"
)

var appConfig = constant.GetConfig()

func AuthRedirect(c echo.Context) error {
	return c.Redirect(301, "https://account.it.chula.ac.th/html/login.html?service=http://"+appConfig.ExternalName+appConfig.CallbackPath+"?state="+c.QueryParam("redirect_uri")+"|"+c.QueryParam("state")+"&serviceName="+c.QueryParam("client_id"))
}

func CallBackRedirect(c echo.Context) error {
	states := strings.Split(c.QueryParam("state"), "|")
	originCallback := states[0]
	state := states[1]
	return c.Redirect(301, originCallback+"?state="+state+"&code="+c.QueryParam("ticket"))
}
