package main

import (
	"github.com/labstack/echo"
	"gitlab.com/freshy-the-series-architecture/chula-sso-oauth-adapter/constant"
	"gitlab.com/freshy-the-series-architecture/chula-sso-oauth-adapter/handler"

	"github.com/labstack/echo/middleware"
)

var appConfig = constant.GetConfig()

func main() {
	e := echo.New()

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.GET(appConfig.AuthPath, handler.AuthRedirect)
	e.GET(appConfig.CallbackPath, handler.CallBackRedirect)
	e.POST(appConfig.TokenPath, handler.TokenValidate)

	e.Logger.Fatal(e.Start(appConfig.Port))
}
